/*eslint-env node */

import babel from 'rollup-plugin-babel';

var external = Object.keys(require('./package.json').dependencies || {}).concat('path');

export default{
  entry: 'lib/index.js',
  dest: 'dist/simplewebcrypto.umd.js',
  format: 'umd',
  external: external,
  plugins: [babel()],
  moduleName: 'SimpleWebCrypto'
};
