/*eslint-env mocha */
/*globals assert, SimpleWebCrypto*/
'use strict';

let plain = 'to buffer converts';

describe('ECDH', function() {
  let Ecdh = SimpleWebCrypto.Ecdh;
  let utils = SimpleWebCrypto.utils;

  it('empty then should auto resolve', done => {
    var algorithm = new Ecdh();
    algorithm.go().then(() => {
      assert.ok(1, 'Resolved');
      done();
    });
  });

  describe('generateKey', function() {
    it('should successfully generate key with nothing', done => {
      var algorithm = new Ecdh();
      algorithm.generateKey().go().then(result => {
        console.log(`generated publicKey ${result.publicKey}`);
        console.log(`generated privateKey ${result.privateKey}`);
        assert.ok(result.publicKey, 'Key generated successfully');
        assert.ok(result.privateKey, 'Key generated successfully');
        done();
      }).catch(err => done(err));
    });
  });

  describe('importKey', function() {
    it('JWK should import successfully', done => {
      let key = {
        kty: 'EC',
        crv: 'P-256',
        x: 'kgR_PqO07L8sZOBbw6rvv7O_f7clqDeiE3WnMkb5EoI',
        y: 'djI-XqCqSyO9GFk_QT_stROMCAROIvU8KOORBgQUemE',
        d: '5aPFSt0UFVXYGu-ZKyC9FQIUOAMmnjzdIwkxCMe3Iok',
        ext: true,
      };

      let algorithm = new Ecdh();
      algorithm.importKey(key, {options: ['deriveKey', 'deriveBits']}).go().then(result => {
        assert.ok(result, 'Key imported successfully');
        done();
      }).catch(err => done(err));
    });

    it('empty should error out', done => {
      let algorithm = new Ecdh();

      algorithm.importKey().go().catch(err => {
        console.error(err);
        assert.ok(err, 'Error thrown');
        done();
      });
    });
  });

  describe('exportKey', function() {
    it('should successfully export with nothing', done => {
      let algorithm = new Ecdh();

      algorithm.generateKey({
        extractable: true
      }).exportKey().go().then(result => {
        console.log(`exported key ${result}`);
        assert.ok(result, 'Key exported successfully');
        done();
      }).catch(err => done(err));
    });

    it('non-extractable should pass for public key', done => {
      let algorithm = new Ecdh();

      algorithm.generateKey().exportKey().go().then(result => {
        console.log(`exported key ${result}`);
        assert.ok(result, 'Key exported successfully');
        done();
      }).catch(err => done(err));
    });

    it('non-extractable should error out for private', done => {

    });
  });

  describe('derive', function() {

    it('should derive successfully', done => {
      let algorithm = new Ecdh();

      algorithm.generateKey().deriveKey().go().then(result => {
        console.log(`exported key ${result}`);
        assert.ok(result, 'Key exported successfully');
        done();
      }).catch(err => done(err));
    });

    it('should derive successfully after import', done => {
      let algorithm = new Ecdh();

      algorithm.generateKey({
        extractable: true
      }).exportKey().go().then(keydata => {
        algorithm.importKey(keydata).deriveKey().go().then(result => {
          console.log(`exported key ${result}`);
          assert.ok(result, 'Key exported successfully');
          done();
        });
      }).catch(err => done(err));
    });
  });

  describe('encrypt', function() {
    it('should decrypt successfully', done => {

    });
  });

  describe('decrypt', function() {
    it('should decrypt successfully', done => {

    });
  });
});
