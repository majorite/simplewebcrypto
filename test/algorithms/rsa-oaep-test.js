/*eslint-env mocha */
/*eslint no-console: 1*/
/*globals assert, SimpleWebCrypto*/
'use strict';
describe('RSA-OAEP', function() {
  let plain = 'to buffer converts';

  let RsaOaep = SimpleWebCrypto.RsaOaep;
  let utils = SimpleWebCrypto.utils;

  it('empty then should auto resolve', done => {
    let algorithm = new RsaOaep();
    algorithm.go().then(() => {
      assert.ok(1, 'Resolved');
      done();
    });
  });

  describe('generateKey', function() {
    it('should successfully generate key with nothing', done => {
      let algorithm = new RsaOaep();

      algorithm.generateKey().go().then(result => {
        console.log(`generated keys ${result.publicKey} - ${result.privateKey}`);
        assert.ok(result, 'Key generated successfully');
        done();
      }).catch(err => done(err));
    });
  });

  describe('importKey', function() {
    it('JWK should import successfully', done => {
      let key = { //this is an example jwk key, other key types are Uint8Array objects
        kty: 'RSA',
        e: 'AQAB',
        n: 'vGO3eU16ag9zRkJ4AK8ZUZrjbtp5xWK0LyFMNT8933evJoHeczexMUzSiXaLrEFSyQZortk81zJH3y41MBO_UFDO_X0crAquNrkjZDrf9Scc5-MdxlWU2Jl7Gc4Z18AC9aNibWVmXhgvHYkEoFdLCFG-2Sq-qIyW4KFkjan05IE',
        alg: 'RSA-OAEP-256',
        ext: true,
      };

      let algorithm = new RsaOaep();
      algorithm.importKey(key).go().then(result => {
        assert.ok(result, 'Key imported successfully');
        done();
      }).catch(err => done(err));
    });

    it('empty should error out', done => {
      let algorithm = new RsaOaep();

      algorithm.importKey().go().catch(err => {
        console.error(err);
        assert.ok(err, 'Error thrown');
        done();
      });
    });
  });

  describe('encrypt', function() {
    it('imported JWK should encrypt successfully', done => {
      let key = { //this is an example jwk key, other key types are Uint8Array objects
        kty: 'RSA',
        e: 'AQAB',
        n: 'vGO3eU16ag9zRkJ4AK8ZUZrjbtp5xWK0LyFMNT8933evJoHeczexMUzSiXaLrEFSyQZortk81zJH3y41MBO_UFDO_X0crAquNrkjZDrf9Scc5-MdxlWU2Jl7Gc4Z18AC9aNibWVmXhgvHYkEoFdLCFG-2Sq-qIyW4KFkjan05IE',
        alg: 'RSA-OAEP-256',
        ext: true
      };

      let algorithm = new RsaOaep();
      let ua = utils.string.toBuffer(plain);

      algorithm.importKey(key).encrypt({
        dataToEncrypt: ua
      }).go().then(response => {
        assert.ok(response, 'Value encrypted successfully');

        done();
      }).catch(err => done(err));
    });
  });

  describe('decrypt', function(){
    it('should decrypt successfully', done => {
      let algorithm = new RsaOaep();
      let ua = utils.string.toBuffer(plain);

      algorithm.generateKey().encrypt({dataToEncrypt: ua}).go().then(encryptedData => {
        assert.ok(encryptedData, 'Value encrypted successfully');

        algorithm.decrypt({dataToDecrypt: encryptedData}).go().then(decryptedData => {
          assert.ok(decryptedData, 'Value decrypted successfully');

          let oldString = utils.string.fromBuffer(decryptedData);

          assert.equal(oldString, plain);
          console.log(oldString);
          done();
        });
      }).catch(err => done(err));
    });
  });
});
