/*eslint-env mocha */
/*globals assert, SimpleWebCrypto*/
'use strict';
describe('AES-GCM', function() {
  let plain = 'to buffer converts';

  let AesGcm = SimpleWebCrypto.AesGcm;
  let utils = SimpleWebCrypto.utils;

  it('empty then should auto resolve', done => {
    let algorithm = new AesGcm();
    algorithm.go().then(() => {
      assert.ok(1, 'Resolved');
      done();
    });
  });

  describe('generateKey', function() {
    it('should successfully generate key with nothing', done => {
      let algorithm = new AesGcm();

      algorithm.generateKey().go().then(result => {
        console.log(`generated key ${result}`);
        assert.ok(result, 'Key generated successfully');
        done();
      }).catch(err => done(err));
    });
  });

  describe('importKey', function() {
    it('Uint8Array should import successfully', done => {
      let aesKey = new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6]);

      let algorithm = new AesGcm();
      algorithm.importKey(aesKey).go().then(result => {
        assert.ok(result, 'Key imported successfully');
        done();
      }).catch(err => done(err));
    });

    it('empty should error out', done => {
      let algorithm = new AesGcm();

      algorithm.importKey().go().catch(err => {
        console.error(err);
        assert.ok(err, 'Error thrown');
        done();
      });
    });
  });

  describe('exportKey', function() {
    it('should successfully export with nothing', done => {
      let algorithm = new AesGcm();

      algorithm.generateKey({
        extractable: true
      }).exportKey().go().then(result => {
        console.log(`exported key ${result}`);
        assert.ok(result, 'Key exported successfully');
        done();
      }).catch(err => done(err));
    });

    it('non-extractable should error out', done => {
      let algorithm = new AesGcm();

      algorithm.generateKey().exportKey().go().catch(err => {
        console.error(err);
        assert.ok(err, 'Error thrown');
        done();
      });
    });
  });

  describe('encrypt', function() {
    it('should encrypt successfully', done => {
      let aesKey = new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6]);
      let algorithm = new AesGcm();
      let ua = utils.string.toBuffer(plain);

      algorithm.importKey(aesKey).encrypt({
        dataToEncrypt: ua
      }).go().then(response => {
        assert.ok(response, 'Value encrypted successfully');

        done();
      }).catch(err => done(err));
    });
  });

  describe('decrypt', function() {
    it('should decrypt successfully', done => {
      let aesKey = new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6]);
      let algorithm = new AesGcm();
      let ua = utils.string.toBuffer(plain);

      algorithm.importKey(aesKey).encrypt({
        dataToEncrypt: ua
      }).go().then(encryptedData => {
        assert.ok(encryptedData, 'Value encrypted successfully');

        algorithm.decrypt({
          dataToDecrypt: encryptedData
        }).go().then(decryptedData => {
          assert.ok(decryptedData, 'Value decrypted successfully');

          let oldString = utils.string.fromBuffer(decryptedData);

          assert.equal(oldString, plain);
          console.log(oldString);
          done();
        });
      }).catch(err => done(err));
    });
  });
});
