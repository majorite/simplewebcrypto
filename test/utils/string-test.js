/*eslint-env mocha */
/*globals assert, SimpleWebCrypto */

'use strict';

describe('String Utils', function(){
  let utils = SimpleWebCrypto.utils.string;

  it('to buffer converts', () => {
    var ua = utils.toBuffer('to buffer converts');
    assert.ok(ua instanceof Uint8Array);
  });

  it('to string converts successfully', () => {
    var ua = utils.toBuffer('to buffer converts');
    var str = utils.fromBuffer(ua);

    assert.ok(typeof str === 'string');
    assert.ok(str === 'to buffer converts');
  });
});
