import utils from '../utils/utils';

class Algorithm {
  constructor({
    algorithm, length, keyType
  } = {}) {
    this.algorithm = algorithm;
    this.length = length;
    this.keyType = keyType;
    this.tasks = [];
  }

  go() {
    let tasks = this.tasks.length > 0 ? utils.promise.waterfall(this.tasks) : Promise.resolve();
    this.tasks.length = 0;
    return tasks;
  }
}

export default Algorithm;
