import utils from '../utils/utils';
import Algorithm from './algorithm';

let _crypto = window.crypto || window.msCrypto;
let _cryptoSubtle = _crypto.subtle || _crypto.webkitSubtle;
let wrap = utils.promise.wrap;

class AesCbc extends Algorithm {
  constructor({
    algorithm = 'AES-CBC', length = 128, ivLength = 16, keyType = 'raw'
  } = {}) {
    super({algorithm, length, keyType});

    this.ivLength = ivLength;
    this.iv = _crypto.getRandomValues(new Uint8Array(ivLength));
  }

  generateKey({
    keyLength, extractable = false, options = ['encrypt', 'decrypt']
  } = {}) {
    keyLength = keyLength || this.length;

    let task = () => {
      return wrap(_cryptoSubtle.generateKey({
        name: this.algorithm,
        length: keyLength
      }, extractable, options)).then(key => {
        this.key = key;
        return key;
      });
    };

    this.tasks.push(task);

    return this;
  }

  importKey(key, {
    extractable = false, options = ['encrypt', 'decrypt']
  } = {}) {

    let task = () => {
      if (!key) return Promise.reject(new Error('Missing key to import.'));

      return wrap(_cryptoSubtle.importKey(this.keyType, key, {
        name: this.algorithm
      }, extractable, options)).then(key => {
        this.key = key;
        return key;
      });
    };

    this.tasks.push(task);

    return this;
  }

  exportKey(key) {

    let task = () => {
      key = key || this.key;

      if (!key) throw new Error('Missing key to export.');

      return wrap(_cryptoSubtle.exportKey(this.keyType, key)).then(key => {
        return utils.array.bufferToUint32(key);
      });
    };

    this.tasks.push(task);

    return this;
  }

  encrypt({key, iv, dataToEncrypt, combine = true} = {}){
    let task = () => {
      key = key || this.key;
      iv = iv || this.iv;

      if(!key || !dataToEncrypt) throw new Error('Missing encryption options.');

      return wrap(_cryptoSubtle.encrypt({
        name: this.algorithm,
        iv: iv
      }, key, dataToEncrypt)).then(encryptedData => {
        return combine ? utils.array.combine(iv, encryptedData): {iv: iv, data: utils.array.bufferToUint32(encryptedData)};
      });
    };

    this.tasks.push(task);

    return this;
  }

  decrypt({
    dataToDecrypt, key
  } = {}){
    let iv, encrypted;

    if(dataToDecrypt instanceof Uint8Array){
      var offset = dataToDecrypt.byteOffset;
      var length = dataToDecrypt.length;

      iv = new Uint8Array(dataToDecrypt.buffer, offset, 16);
      encrypted = new Uint8Array(dataToDecrypt.buffer, offset + 16, length - 16);
    }else{
      iv = new Uint8Array(dataToDecrypt, 0, 16);
      encrypted = new Uint8Array(dataToDecrypt, 16);
    }

    let task = () => {
      key = key || this.key;

      return wrap(_cryptoSubtle.decrypt({
        name: this.algorithm,
        iv: iv
      }, key, encrypted)).then(decryptedData => {
        return utils.array.bufferToUint32(decryptedData);
      });
    };

    this.tasks.push(task);

    return this;
  }
}

export default AesCbc;
