import utils from '../utils/utils';
import Algorithm from './algorithm';

let _crypto = window.crypto || window.msCrypto;
let _cryptoSubtle = _crypto.subtle || _crypto.webkitSubtle;
let wrap = utils.promise.wrap;

class RsaOaep extends Algorithm {
  constructor({
    algorithm = 'RSA-OAEP',
    length = 2048,
    publicExponent = new Uint8Array([0x01, 0x00, 0x01]),
    hash = 'SHA-256',
    keyType = 'jwk'
  } = {}) {
    super({
      algorithm,
      length,
      keyType
    });
    this.publicExponent = publicExponent;
    this.hash = hash;
  }

  generateKey({
    keyLength,
    extractable = false,
    options = ['encrypt', 'decrypt'],
    hash
  } = {}) {
    keyLength = keyLength || this.length;
    hash = hash || this.hash;

    let task = () => {
      return wrap(_cryptoSubtle.generateKey({
        name: this.algorithm,
        modulusLength: keyLength,
        publicExponent: this.publicExponent,
        hash: {
          name: hash
        }
      }, extractable, options)).then(key => {
        this.key = key;
        this.publicKey = key.publicKey;
        this.privateKey = key.privateKey;

        return key;
      });
    };

    this.tasks.push(task);

    return this;
  }

  importKey(key, {
    extractable = false,
    options = ['encrypt'],
    hash
  } = {}) {
    hash = hash || this.hash;

    let task = () => {
      if (!key) return Promise.reject(new Error('Missing key to import.'));

      return wrap(_cryptoSubtle.importKey(this.keyType, key, {
        name: this.algorithm,
        hash: {
          name: hash
        }
      }, extractable, options)).then(key => {
        if(key.type === 'private'){
          this.privateKey = key;
        }
        else
          this.publicKey = key;

        return key;
      });
    };

    this.tasks.push(task);

    return this;
  }

  exportKey(key) {

    let task = () => {
      key = key || this.key;

      if (!key) throw new Error('Missing key to export.');

      return wrap(_cryptoSubtle.exportKey(this.keyType, key)).then(key => {
        return utils.array.bufferToUint32(key);
      });
    };

    this.tasks.push(task);

    return this;
  }

  encrypt({key, dataToEncrypt} = {}){
    let task = () => {
      key = key || this.publicKey;

      if(!key || !dataToEncrypt) throw new Error('Missing encryption options.');

      return wrap(_cryptoSubtle.encrypt({
        name: this.algorithm
      }, key, dataToEncrypt)).then(encryptedData => {
        return new Uint8Array(encryptedData);
      });
    };

    this.tasks.push(task);

    return this;
  }

  decrypt({
    dataToDecrypt, key
  } = {}){
    key = key || this.privateKey;

    let task = () => {
      return wrap(_cryptoSubtle.decrypt({
        name: this.algorithm
      }, key, dataToDecrypt)).then(decryptedData => {
        return utils.array.bufferToUint32(decryptedData);
      });
    };

    this.tasks.push(task);

    return this;
  }
}

export default RsaOaep;
