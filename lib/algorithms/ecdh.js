import utils from '../utils/utils';

let _crypto = window.crypto || window.msCrypto;
let _cryptoSubtle = _crypto.subtle || _crypto.webkitSubtle;
let wrap = utils.promise.wrap;

class Ecdh {
  constructor({
    algorithm = 'ECDH',
    namedCurve = 'P-256',
    derivedAlgorithm = 'AES-GCM',
    derivedLength = '256',
    keyType = 'jwk'
  } = {}) {

    this.algorithm = algorithm;
    this.namedCurve = namedCurve;
    this.derivedLength = derivedLength;
    this.derivedAlgorithm = derivedAlgorithm;
    this.keyType = keyType;
    this.tasks = [];
  }

  generateKey({
    extractable = false,
    options = ['deriveKey', 'deriveBits']
  } = {}) {

    let task = () => {
      return wrap(_cryptoSubtle.generateKey({
        name: this.algorithm,
        namedCurve: this.namedCurve
      }, extractable, options)).then(key => {
        this.key = key;
        return key;
      });
    };

    this.tasks.push(task);

    return this;
  }

  importKey(key, {
    extractable = false,
    options = []
  } = {}) {

    let task = () => {
      if (!key) return Promise.reject(new Error('Missing key to import.'));

      return wrap(_cryptoSubtle.importKey(this.keyType, key, {
        name: this.algorithm,
        namedCurve: this.namedCurve
      }, extractable, options)).then(key => {
        if(options.length == 0){
          this.publicKey = key;
        }
        else {
          this.privateKey = key;
        }
        return key;
      });
    };

    this.tasks.push(task);

    return this;
  }

  exportKey(key) {

    let task = () => {
      key = key || this.key.publicKey;

      if (!key) throw new Error('Missing key to export.');

      return wrap(_cryptoSubtle.exportKey(this.keyType, key)).then(key => {
        return key instanceof ArrayBuffer ? utils.array.bufferToUint32(key) : key;
      });
    };

    this.tasks.push(task);

    return this;
  }

  deriveKey({
      extractable = false,
      options = ['encrypt', 'decrypt']
  } = {}) {
    let task = () => {
      let publicKey = this.key.publicKey;
      let privateKey = this.key.privateKey;
      let derivedAlgorithm = this.derivedAlgorithm;
      let derivedLength = this.derivedLength;

      if (!publicKey) throw new Error('Missing publicKey to derive.');
      if (!privateKey) throw new Error('Missing privateKey to derive.');

      return wrap(_cryptoSubtle.deriveKey({
            name: this.algorithm,
            namedCurve: this.namedCurve,
            public: publicKey
          },
          privateKey, {
            name: derivedAlgorithm,
            length: derivedLength
          }, extractable, options))
        .then(key => {
          this.derivedKey = key;
          return key;
        });
    };

    this.tasks.push(task);

    return this;
  }

  go() {
    let tasks = this.tasks.length > 0 ? utils.promise.waterfall(this.tasks) : Promise.resolve();
    this.tasks.length = 0;
    return tasks;
  }
}

export default Ecdh;
