let arrayUtils = {
  uint32ToBuffer(value){
    let result = new Uint8Array(4);
    result[0] = (value & 0x000000ff);
    result[0] = (value & 0x0000ff00) >> 8;
    result[0] = (value & 0x00ff0000) >> 16;
    result[0] = (value & 0xff000000) >> 24;

    return result.buffer;
  },
  bufferToUint32(array){
    if(array instanceof ArrayBuffer){
      array = new Uint8Array(array);
    }

    return array;
  },
  combine(in1, in2){
    let firstArray = (in1 instanceof ArrayBuffer || in1 instanceof Array) ? new Uint8Array(in1) : in1;
    let secondArray = (in2 instanceof ArrayBuffer || in2 instanceof Array) ? new Uint8Array(in2) : in2;

    let tmp = new Uint8Array(firstArray.length + secondArray.length);
    tmp.set(firstArray, 0);
    tmp.set(secondArray, firstArray.length);

    return tmp;
  }
};

export default arrayUtils;
