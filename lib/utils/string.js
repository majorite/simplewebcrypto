let stringUtils = {
  toBuffer(str) {
    var ua = new Uint8Array(str.length);
    for (var i = 0, j = str.length; i < j; ++i) {
      ua[i] = str.charCodeAt(i);
    }

    return ua;
  },
  fromBuffer(ua){
    var str = '';
    for(var i = 0; i < ua.length; i++){
      str+= String.fromCharCode(ua[i]);
    }

    return str;
  }
};

export default stringUtils;
