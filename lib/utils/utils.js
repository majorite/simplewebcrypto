import string from './string';
import promise from './promise';
import array from './array';

let oldWebkit = false;
let oldIE = false;

if (window.crypto && window.crypto.webkitSubtle && window.crypto.suble === undefined) {
  oldWebkit = true;
}

if (window.msCrypto && (window.crypto === undefined || window.crypto.subtle === undefined)) {
  oldIE = true;
}

let is = {
  oldWebkit,
  oldIE
};

export default {
  string,
  promise,
  array,
  is
};
