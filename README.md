# simplewebcrypto

## Introduction

simplewebcrypto is an open-source library sporting a high level implementation of the [Web Cryptography API](https://www.w3.org/TR/WebCryptoAPI/).

## Why simplewebcrypto
* Minimal abstraction
* Dead simple syntax
* Chainable by default
* Promise based
* UMD compatible
* Abstracts away vendor complexity

## Supported browsers
Tested browsers: 
* Latest Chrome

## Installation

node
````
TBD
````

bower
````
TBD
````

## Example Usage

````javascript
var aesGcm = new AesGcm();
aesGcm.generateKey().encrypt(someBytes).go().then(encrypted => console.log(encrypted));
````

## Roadmap
1. Implement all [recommended](https://diafygi.github.io/webcrypto-examples/) algorithms
2. Improve test coverage
3. Implement discouraged algorithms for backwards compatability

## Inspiration
1. [webcrypto examples](https://github.com/diafygi/webcrypto-examples)