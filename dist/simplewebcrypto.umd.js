(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (factory((global.SimpleWebCrypto = global.SimpleWebCrypto || {})));
}(this, function (exports) { 'use strict';

  var babelHelpers = {};
  babelHelpers.typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
  };

  babelHelpers.classCallCheck = function (instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  };

  babelHelpers.createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  babelHelpers.inherits = function (subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  };

  babelHelpers.possibleConstructorReturn = function (self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  };

  babelHelpers;

  var stringUtils = {
    toBuffer: function toBuffer(str) {
      var ua = new Uint8Array(str.length);
      for (var i = 0, j = str.length; i < j; ++i) {
        ua[i] = str.charCodeAt(i);
      }

      return ua;
    },
    fromBuffer: function fromBuffer(ua) {
      var str = '';
      for (var i = 0; i < ua.length; i++) {
        str += String.fromCharCode(ua[i]);
      }

      return str;
    }
  };

  function isPromise(obj) {
    return obj && typeof obj.then === 'function';
  }

  function waterfall(list) {
    // malformed argument
    list = Array.prototype.slice.call(list);
    if (!Array.isArray(list) // not an array
     || typeof list.reduce !== 'function' // update your javascript engine
     || list.length < 1 // empty array
    ) {
        return Promise.reject('Array with reduce function is needed.');
      }

    if (list.length == 1) {
      if (typeof list[0] != 'function') return Promise.reject('First element of the array should be a function, got ' + babelHelpers.typeof(list[0]));
      return Promise.resolve(list[0]());
    }

    return list.reduce(function (l, r) {
      // first round
      // execute function and return promise
      var isFirst = l == list[0];
      if (isFirst) {
        if (typeof l != 'function') return Promise.reject('List elements should be function to call.');

        var lret = l();
        if (!isPromise(lret)) return Promise.reject('Function return value should be a promise.');else return lret.then(r);
      }

      // other rounds
      // l is a promise now
      // priviousPromiseList.then(nextFunction)
      else {
          if (!isPromise(l)) Promise.reject('Function return value should be a promise.');else return l.then(r);
        }
    });
  }

  function wrap$1(result) {
    if (typeof result.then === 'function') {
      return result;
    } else {
      return new Promise(function (resolve, reject) {
        result.onerror = function (event) {
          return reject(event.target.result);
        };

        result.oncomplete = function (event) {
          return resolve(event.target.result);
        };
      });
    }
  }

  var promise = {
    waterfall: waterfall,
    wrap: wrap$1
  };

  var arrayUtils = {
    uint32ToBuffer: function uint32ToBuffer(value) {
      var result = new Uint8Array(4);
      result[0] = value & 0x000000ff;
      result[0] = (value & 0x0000ff00) >> 8;
      result[0] = (value & 0x00ff0000) >> 16;
      result[0] = (value & 0xff000000) >> 24;

      return result.buffer;
    },
    bufferToUint32: function bufferToUint32(array) {
      if (array instanceof ArrayBuffer) {
        array = new Uint8Array(array);
      }

      return array;
    },
    combine: function combine(in1, in2) {
      var firstArray = in1 instanceof ArrayBuffer || in1 instanceof Array ? new Uint8Array(in1) : in1;
      var secondArray = in2 instanceof ArrayBuffer || in2 instanceof Array ? new Uint8Array(in2) : in2;

      var tmp = new Uint8Array(firstArray.length + secondArray.length);
      tmp.set(firstArray, 0);
      tmp.set(secondArray, firstArray.length);

      return tmp;
    }
  };

  var oldWebkit = false;
  var oldIE = false;

  if (window.crypto && window.crypto.webkitSubtle && window.crypto.suble === undefined) {
    oldWebkit = true;
  }

  if (window.msCrypto && (window.crypto === undefined || window.crypto.subtle === undefined)) {
    oldIE = true;
  }

  var is = {
    oldWebkit: oldWebkit,
    oldIE: oldIE
  };

  var utils = {
    string: stringUtils,
    promise: promise,
    array: arrayUtils,
    is: is
  };

  var Algorithm = function () {
    function Algorithm() {
      var _ref = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

      var algorithm = _ref.algorithm;
      var length = _ref.length;
      var keyType = _ref.keyType;
      babelHelpers.classCallCheck(this, Algorithm);

      this.algorithm = algorithm;
      this.length = length;
      this.keyType = keyType;
      this.tasks = [];
    }

    babelHelpers.createClass(Algorithm, [{
      key: 'go',
      value: function go() {
        var tasks = this.tasks.length > 0 ? utils.promise.waterfall(this.tasks) : Promise.resolve();
        this.tasks.length = 0;
        return tasks;
      }
    }]);
    return Algorithm;
  }();

  var _crypto = window.crypto || window.msCrypto;
  var _cryptoSubtle = _crypto.subtle || _crypto.webkitSubtle;
  var wrap = utils.promise.wrap;

  var AesGcm = function (_Algorithm) {
    babelHelpers.inherits(AesGcm, _Algorithm);

    function AesGcm() {
      var _ref = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

      var _ref$algorithm = _ref.algorithm;
      var algorithm = _ref$algorithm === undefined ? 'AES-GCM' : _ref$algorithm;
      var _ref$length = _ref.length;
      var length = _ref$length === undefined ? 128 : _ref$length;
      var _ref$ivLength = _ref.ivLength;
      var ivLength = _ref$ivLength === undefined ? 16 : _ref$ivLength;
      var _ref$keyType = _ref.keyType;
      var keyType = _ref$keyType === undefined ? 'raw' : _ref$keyType;
      babelHelpers.classCallCheck(this, AesGcm);

      var _this = babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(AesGcm).call(this, { algorithm: algorithm, length: length, keyType: keyType }));

      _this.ivLength = ivLength;
      _this.iv = _crypto.getRandomValues(new Uint8Array(ivLength));
      return _this;
    }

    babelHelpers.createClass(AesGcm, [{
      key: 'generateKey',
      value: function generateKey() {
        var _this2 = this;

        var _ref2 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        var keyLength = _ref2.keyLength;
        var _ref2$extractable = _ref2.extractable;
        var extractable = _ref2$extractable === undefined ? false : _ref2$extractable;
        var _ref2$options = _ref2.options;
        var options = _ref2$options === undefined ? ['encrypt', 'decrypt'] : _ref2$options;

        keyLength = keyLength || this.length;

        var task = function task() {
          return wrap(_cryptoSubtle.generateKey({
            name: _this2.algorithm,
            length: keyLength
          }, extractable, options)).then(function (key) {
            _this2.key = key;
            return key;
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'importKey',
      value: function importKey(key) {
        var _this3 = this;

        var _ref3 = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

        var _ref3$extractable = _ref3.extractable;
        var extractable = _ref3$extractable === undefined ? false : _ref3$extractable;
        var _ref3$options = _ref3.options;
        var options = _ref3$options === undefined ? ['encrypt', 'decrypt'] : _ref3$options;


        var task = function task() {
          if (!key) return Promise.reject(new Error('Missing key to import.'));

          return wrap(_cryptoSubtle.importKey(_this3.keyType, key, {
            name: _this3.algorithm
          }, extractable, options)).then(function (key) {
            _this3.key = key;
            return key;
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'exportKey',
      value: function exportKey(key) {
        var _this4 = this;

        var task = function task() {
          key = key || _this4.key;

          if (!key) throw new Error('Missing key to export.');

          return wrap(_cryptoSubtle.exportKey(_this4.keyType, key)).then(function (key) {
            return utils.array.bufferToUint32(key);
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'encrypt',
      value: function encrypt() {
        var _this5 = this;

        var _ref4 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        var key = _ref4.key;
        var iv = _ref4.iv;
        var dataToEncrypt = _ref4.dataToEncrypt;
        var _ref4$combine = _ref4.combine;
        var combine = _ref4$combine === undefined ? true : _ref4$combine;

        var task = function task() {
          key = key || _this5.key;
          iv = iv || _this5.iv;

          if (!key || !dataToEncrypt) throw new Error('Missing encryption options.');

          return wrap(_cryptoSubtle.encrypt({
            name: _this5.algorithm,
            iv: iv
          }, key, dataToEncrypt)).then(function (encryptedData) {
            return combine ? utils.array.combine(iv, encryptedData) : { iv: iv, data: utils.array.bufferToUint32(encryptedData) };
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'decrypt',
      value: function decrypt() {
        var _this6 = this;

        var _ref5 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        var dataToDecrypt = _ref5.dataToDecrypt;
        var key = _ref5.key;

        var iv = void 0,
            encrypted = void 0;
        key = key || this.key;

        if (dataToDecrypt instanceof Uint8Array) {
          var offset = dataToDecrypt.byteOffset;
          var length = dataToDecrypt.length;

          iv = new Uint8Array(dataToDecrypt.buffer, offset, 16);
          encrypted = new Uint8Array(dataToDecrypt.buffer, offset + 16, length - 16);
        } else {
          iv = new Uint8Array(dataToDecrypt, 0, 16);
          encrypted = new Uint8Array(dataToDecrypt, 16);
        }

        var task = function task() {
          return wrap(_cryptoSubtle.decrypt({
            name: _this6.algorithm,
            iv: iv
          }, key, encrypted)).then(function (decryptedData) {
            return utils.array.bufferToUint32(decryptedData);
          });
        };

        this.tasks.push(task);

        return this;
      }
    }]);
    return AesGcm;
  }(Algorithm);

  var _crypto$1 = window.crypto || window.msCrypto;
  var _cryptoSubtle$1 = _crypto$1.subtle || _crypto$1.webkitSubtle;
  var wrap$2 = utils.promise.wrap;

  var AesCbc = function (_Algorithm) {
    babelHelpers.inherits(AesCbc, _Algorithm);

    function AesCbc() {
      var _ref = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

      var _ref$algorithm = _ref.algorithm;
      var algorithm = _ref$algorithm === undefined ? 'AES-CBC' : _ref$algorithm;
      var _ref$length = _ref.length;
      var length = _ref$length === undefined ? 128 : _ref$length;
      var _ref$ivLength = _ref.ivLength;
      var ivLength = _ref$ivLength === undefined ? 16 : _ref$ivLength;
      var _ref$keyType = _ref.keyType;
      var keyType = _ref$keyType === undefined ? 'raw' : _ref$keyType;
      babelHelpers.classCallCheck(this, AesCbc);

      var _this = babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(AesCbc).call(this, { algorithm: algorithm, length: length, keyType: keyType }));

      _this.ivLength = ivLength;
      _this.iv = _crypto$1.getRandomValues(new Uint8Array(ivLength));
      return _this;
    }

    babelHelpers.createClass(AesCbc, [{
      key: 'generateKey',
      value: function generateKey() {
        var _this2 = this;

        var _ref2 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        var keyLength = _ref2.keyLength;
        var _ref2$extractable = _ref2.extractable;
        var extractable = _ref2$extractable === undefined ? false : _ref2$extractable;
        var _ref2$options = _ref2.options;
        var options = _ref2$options === undefined ? ['encrypt', 'decrypt'] : _ref2$options;

        keyLength = keyLength || this.length;

        var task = function task() {
          return wrap$2(_cryptoSubtle$1.generateKey({
            name: _this2.algorithm,
            length: keyLength
          }, extractable, options)).then(function (key) {
            _this2.key = key;
            return key;
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'importKey',
      value: function importKey(key) {
        var _this3 = this;

        var _ref3 = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

        var _ref3$extractable = _ref3.extractable;
        var extractable = _ref3$extractable === undefined ? false : _ref3$extractable;
        var _ref3$options = _ref3.options;
        var options = _ref3$options === undefined ? ['encrypt', 'decrypt'] : _ref3$options;


        var task = function task() {
          if (!key) return Promise.reject(new Error('Missing key to import.'));

          return wrap$2(_cryptoSubtle$1.importKey(_this3.keyType, key, {
            name: _this3.algorithm
          }, extractable, options)).then(function (key) {
            _this3.key = key;
            return key;
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'exportKey',
      value: function exportKey(key) {
        var _this4 = this;

        var task = function task() {
          key = key || _this4.key;

          if (!key) throw new Error('Missing key to export.');

          return wrap$2(_cryptoSubtle$1.exportKey(_this4.keyType, key)).then(function (key) {
            return utils.array.bufferToUint32(key);
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'encrypt',
      value: function encrypt() {
        var _this5 = this;

        var _ref4 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        var key = _ref4.key;
        var iv = _ref4.iv;
        var dataToEncrypt = _ref4.dataToEncrypt;
        var _ref4$combine = _ref4.combine;
        var combine = _ref4$combine === undefined ? true : _ref4$combine;

        var task = function task() {
          key = key || _this5.key;
          iv = iv || _this5.iv;

          if (!key || !dataToEncrypt) throw new Error('Missing encryption options.');

          return wrap$2(_cryptoSubtle$1.encrypt({
            name: _this5.algorithm,
            iv: iv
          }, key, dataToEncrypt)).then(function (encryptedData) {
            return combine ? utils.array.combine(iv, encryptedData) : { iv: iv, data: utils.array.bufferToUint32(encryptedData) };
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'decrypt',
      value: function decrypt() {
        var _this6 = this;

        var _ref5 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        var dataToDecrypt = _ref5.dataToDecrypt;
        var key = _ref5.key;

        var iv = void 0,
            encrypted = void 0;

        if (dataToDecrypt instanceof Uint8Array) {
          var offset = dataToDecrypt.byteOffset;
          var length = dataToDecrypt.length;

          iv = new Uint8Array(dataToDecrypt.buffer, offset, 16);
          encrypted = new Uint8Array(dataToDecrypt.buffer, offset + 16, length - 16);
        } else {
          iv = new Uint8Array(dataToDecrypt, 0, 16);
          encrypted = new Uint8Array(dataToDecrypt, 16);
        }

        var task = function task() {
          key = key || _this6.key;

          return wrap$2(_cryptoSubtle$1.decrypt({
            name: _this6.algorithm,
            iv: iv
          }, key, encrypted)).then(function (decryptedData) {
            return utils.array.bufferToUint32(decryptedData);
          });
        };

        this.tasks.push(task);

        return this;
      }
    }]);
    return AesCbc;
  }(Algorithm);

  var _crypto$2 = window.crypto || window.msCrypto;
  var _cryptoSubtle$2 = _crypto$2.subtle || _crypto$2.webkitSubtle;
  var wrap$3 = utils.promise.wrap;

  var RsaOaep = function (_Algorithm) {
    babelHelpers.inherits(RsaOaep, _Algorithm);

    function RsaOaep() {
      var _ref = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

      var _ref$algorithm = _ref.algorithm;
      var algorithm = _ref$algorithm === undefined ? 'RSA-OAEP' : _ref$algorithm;
      var _ref$length = _ref.length;
      var length = _ref$length === undefined ? 2048 : _ref$length;
      var _ref$publicExponent = _ref.publicExponent;
      var publicExponent = _ref$publicExponent === undefined ? new Uint8Array([0x01, 0x00, 0x01]) : _ref$publicExponent;
      var _ref$hash = _ref.hash;
      var hash = _ref$hash === undefined ? 'SHA-256' : _ref$hash;
      var _ref$keyType = _ref.keyType;
      var keyType = _ref$keyType === undefined ? 'jwk' : _ref$keyType;
      babelHelpers.classCallCheck(this, RsaOaep);

      var _this = babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(RsaOaep).call(this, {
        algorithm: algorithm,
        length: length,
        keyType: keyType
      }));

      _this.publicExponent = publicExponent;
      _this.hash = hash;
      return _this;
    }

    babelHelpers.createClass(RsaOaep, [{
      key: 'generateKey',
      value: function generateKey() {
        var _this2 = this;

        var _ref2 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        var keyLength = _ref2.keyLength;
        var _ref2$extractable = _ref2.extractable;
        var extractable = _ref2$extractable === undefined ? false : _ref2$extractable;
        var _ref2$options = _ref2.options;
        var options = _ref2$options === undefined ? ['encrypt', 'decrypt'] : _ref2$options;
        var hash = _ref2.hash;

        keyLength = keyLength || this.length;
        hash = hash || this.hash;

        var task = function task() {
          return wrap$3(_cryptoSubtle$2.generateKey({
            name: _this2.algorithm,
            modulusLength: keyLength,
            publicExponent: _this2.publicExponent,
            hash: {
              name: hash
            }
          }, extractable, options)).then(function (key) {
            _this2.key = key;
            _this2.publicKey = key.publicKey;
            _this2.privateKey = key.privateKey;

            return key;
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'importKey',
      value: function importKey(key) {
        var _this3 = this;

        var _ref3 = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

        var _ref3$extractable = _ref3.extractable;
        var extractable = _ref3$extractable === undefined ? false : _ref3$extractable;
        var _ref3$options = _ref3.options;
        var options = _ref3$options === undefined ? ['encrypt'] : _ref3$options;
        var hash = _ref3.hash;

        hash = hash || this.hash;

        var task = function task() {
          if (!key) return Promise.reject(new Error('Missing key to import.'));

          return wrap$3(_cryptoSubtle$2.importKey(_this3.keyType, key, {
            name: _this3.algorithm,
            hash: {
              name: hash
            }
          }, extractable, options)).then(function (key) {
            if (key.type === 'private') {
              _this3.privateKey = key;
            } else _this3.publicKey = key;

            return key;
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'exportKey',
      value: function exportKey(key) {
        var _this4 = this;

        var task = function task() {
          key = key || _this4.key;

          if (!key) throw new Error('Missing key to export.');

          return wrap$3(_cryptoSubtle$2.exportKey(_this4.keyType, key)).then(function (key) {
            return utils.array.bufferToUint32(key);
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'encrypt',
      value: function encrypt() {
        var _this5 = this;

        var _ref4 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        var key = _ref4.key;
        var dataToEncrypt = _ref4.dataToEncrypt;

        var task = function task() {
          key = key || _this5.publicKey;

          if (!key || !dataToEncrypt) throw new Error('Missing encryption options.');

          return wrap$3(_cryptoSubtle$2.encrypt({
            name: _this5.algorithm
          }, key, dataToEncrypt)).then(function (encryptedData) {
            return new Uint8Array(encryptedData);
          });
        };

        this.tasks.push(task);

        return this;
      }
    }, {
      key: 'decrypt',
      value: function decrypt() {
        var _this6 = this;

        var _ref5 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        var dataToDecrypt = _ref5.dataToDecrypt;
        var key = _ref5.key;

        key = key || this.privateKey;

        var task = function task() {
          return wrap$3(_cryptoSubtle$2.decrypt({
            name: _this6.algorithm
          }, key, dataToDecrypt)).then(function (decryptedData) {
            return utils.array.bufferToUint32(decryptedData);
          });
        };

        this.tasks.push(task);

        return this;
      }
    }]);
    return RsaOaep;
  }(Algorithm);

  exports.AesGcm = AesGcm;
  exports.AesCbc = AesCbc;
  exports.RsaOaep = RsaOaep;
  exports.utils = utils;

}));